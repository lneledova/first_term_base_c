#include <algorithm>

struct Node {
  int value;
  int diff = 0;
  Node* parent = nullptr;
  Node* left = nullptr;
  Node* right = nullptr;
};

void LeftRotate(Node* a) {
  Node* b = a->right;
  b->parent = a->parent;
  a->parent = b;
  a->right = b->left;
  b->left = a;

  a->diff += 1 - std::min(b->diff, 0);
  b->diff += 1 + std::max(0, a->diff);

  if (b->parent) {
    if (b->parent->left == a) {
      b->parent->left = b;
    } else {
      b->parent->right = b;
    }
  }
}

void RotateRight(Node*);

void BigRotateLeft(Node* a) {
  RotateRight(a->right);
  LeftRotate(a);
}