#ifndef POINT_H
#define POINT_H
#include <cstdint>
#include <string>
#include "shape.h"

namespace geometry {
  class Vector;

  class Segment;

  class Point : public IShape {
    private:
      int64_t coord_x_;
      int64_t coord_y_;
    
    public:
      Point() = default;
      Point(int64_t x, int64_t y) : coord_x_(x), coord_y_(y) {};

      Point& Move(const Vector& shift) override;
      bool ContainsPoint(const Point&) const override;
      bool CrossesSegment(const Segment&) const override;
      Point* Clone() const override;
      std::string ToString() const override;

  };
}


#endif