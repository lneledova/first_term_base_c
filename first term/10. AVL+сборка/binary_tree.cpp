#include <iostream>

struct Node {
  int value;
  Node* parent = nullptr;
  Node* left = nullptr;
  Node* right = nullptr;
};

void Insert(Node* root, Node* new_node) {
  if (root == nullptr) {
    root = new_node;
    return;
  }
  if (root->value < new_node->value) {
    if (root->right == nullptr) {
      root->right = new_node;
      new_node->parent = root;
    } else {
      Insert(root->right, new_node);
    }
  } else {
    if (root->left == nullptr) {
      root->left = new_node;
      new_node->parent = root;
    } else {
      Insert(root->left, new_node);
    }
  }
}

Node* Search(Node* root, int value) {
  if (root == nullptr) {
    return nullptr;
  }
  if (root->value == value) {
    return root;
  }
  if (root->value > value) {
    return Search(root->left, value);
  }
  return Search(root->right, value);
}

// вершина, левое поддерево, правое поддерево,
void Print(Node* root) {
  if (root != nullptr) {
    std::cout << root->value << ' ';
    Print(root->left);
    Print(root->right);
  }
}