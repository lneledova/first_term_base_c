#include <cstddef>

struct Node {
  Node* prev = nullptr;
  int value;
};

class Stack {
 private:
  Node* last = nullptr;
  size_t size = 0;

 public:
  void Push(int val);
  int Pop();
  void Clear();
  int& operator[](int idx);

  int Back() const;
  int Size() const;
  int operator[](int idx) const;
};

int& Stack::operator[](int idx) {
  if (idx > size - 1) {
    return last->value;
  }
  Node* temp = last;
  for (int i = size - 1; i > idx; --i) {
    temp = temp->prev;
  }
  return temp->value;
}

int Stack::operator[](int idx) const {
  if (idx > size - 1) {
    return last->value;
  }
  Node* temp = last;
  for (int i = size - 1; i > idx; --i) {
    temp = temp->prev;
  }
  return temp->value;
}

void Stack::Push(int val) {
  auto new_node = new Node;
  new_node->value = val;
  new_node->prev = this->last;
  this->last = new_node;
  ++this->size;
}

int Stack::Pop() {
  if (this->size == 0) {
    return 0;
  }
  int temp = this->last->value;
  Node* last_for_delete = this->last;
  this->last = this->last->prev;
  delete last_for_delete;
  --this->size;
  return temp;
}

int Stack::Back() const {
  if (this->size == 0) {
    return 0;
  }
  return this->last->value;
}

int Stack::Size() const { return this->size; }

void Stack::Clear() {
  if (this->size > 0) {
    this->Pop();
    this->Clear();
  }
  delete this->last;
}
