#include <iostream>

int Winner(int n) {
  bool* dp = new bool[n + 1];
  dp[0] = false;
  dp[1] = true;
  dp[2] = true;
  for (int i = 3; i < n + 1; ++i) {
    if (i % 3 == 0) {  // взять 1 или 2
      dp[i] = !dp[i - 1] || !dp[i - 2];
    } else if (i % 3 == 1) {  // 1 3
      dp[i] = !dp[i - 1] || !dp[i - 3];
    } else {  // 1 2 3
      dp[i] = !dp[i - 1] || !dp[i - 2] || !dp[i - 3];
    }
  }
  int result = (dp[n] ? 1 : 2);
  delete[] dp;
  return result;
}

int main() {
  int n;
  std::cin >> n;
  std::cout << Winner(n);
}
