# Пользовательские типы данных
## Enum
Перечисление - пользовательский тип данных, значения которого ограничены
набором именованных констант некоторого целого типа.
```
enum Season { kWinter, kSpring, kSummer, kFall };
```
Значения представляют собой литералы (то есть rvalue) некоторого
целочисленного типа (обычно int ).\
Первое значение равно 0, второе - 1, и т.д. (Значения можно указать явно)\
Значения перечислений неявно преобразуются в низлежащий целый тип,
обратные неявные преобразования запрещены.\
Рекомендованным способом объявления перечислений является `enum class` :
```
enum class Season { kWinter, kSpring, kSummer, kFall };

// К значениям enum class можно обратиться только по полному имени:
auto x = Season::kWinter;

// enum class не допускает неявного приведения к низлежащему типу:
int x = Season::kWinter; // CE
Season::kWinter + Season::kFall; // CE

//Как и к пространствам имен к ним применима директива using :
using enum Season;
auto x = kWinter; // Ok
```

## Union
Объединение - пользовательский тип данных, который в каждый момент времени
позволяет хранить объект одного из заранее указанных типов в общей области
памяти.
Короче говоря, это тип, который может хранить разнородные данные (но только
один объект в каждый момент).
```
union IntDouble {
    int i;
    double d;
};

IntDouble x;
x.i = 0; // действующий член - i
x.d = 1.5; // а теперь - d
std::cout << x.d; // Ok
std::cout << x.i; //UB
```

## Структуры
Структура - составной тип данных, который инкапсулирует набор данных
(возможно разных типов) в одном объекте.

```
struct S {
    float y;
    int x = 1; // поле со значением по умолчанию
} a = {1, 2.0}, *ptr; // можно сразу определить переменные

// При присваивании все поля побитово копируются (даже массивы)
S b = a; // все поля a побитово копируются в b
a = b; // присваивание тоже работает

a.y = 0; //  Обращение к элементу структуры
auto ptr = new S; // создаем структуру в динамической памяти
(*ptr).x = 0; // разыменовываем указатель, обращаемся к полю x
//Последнюю строчку можно заменить более простым синтаксисом:
ptr->x = 0; // операция ->
```

```
struct S {
    int x;
    const float y;
};
// Значения константных полей должны быть указаны при инициализации.
S a; // CE
S b = {2}; // Ok (b.y == 0)
S c = {1, 2}; // Ok (c.y == 2)
```
Определение структур включает описание всех полей, входящих в структуру.
В месте использования структуры ее определение должно быть видно (а
действительно ли в этой структуре есть такие поля). Поэтому в месте использования структуры должно быть видно ее определение.
Для этого код структур помещается либо в том же файле, либо в подключаемом
заголовочном файле.\
Правило одного определения для структур такое же, как и для шаблонов:
Допускается не более одного определения одной и той же структуры на
единицу трансляции. При этом все определения в программе должны быть
идентичны.
```
struct S {
    static int x; // Статическое поле
};
// Такие поля принадлежат не конкретному объекту, а всей структуре. То есть это
// поле является общим для всех объектов и хранится в статической (глобальной) области.
S a;
S b;
a.x = 1;
b.x == 1; // true
b.x = 2;
a.x == 2; // true
S::x = 0; // можно обратиться напрямую
```
Но есть нюанс. Статические поля являются видом глобальных переменных, а для
переменных ODR требует ровно одного определения во всей программе. Поэтому
определять статические переменные внутри структур нельзя. Исключение -
константные целочисленные поля:
```
struct S {
    static const int x = 1; // так можно
    static int y = 2; // а так нельзя
};
```
Поэтому определение статического поля должно быть вынесено в отдельный
cpp файл. Либо необходимо пометить статическое поле inline :
```
struct S {
    inline static int x = 1;
};
```
# Стуктуры данных

## Стек
 Абстрактный тип данных, представляющий собой список элементов, организованных по принципу LIFO (last in — first out).\
Традиционно имеет методы:
- push - положить в конец
- pop - достать последний элемент
## Очередь
Aбстрактный тип данных с дисциплиной доступа к элементам FIFO  (first in - first out). \
Традиционные методы: 
- push - добавить в конец
- pop - достать из начала


## [Девятый контест](https://contest.yandex.ru/contest/41866/problems/)
