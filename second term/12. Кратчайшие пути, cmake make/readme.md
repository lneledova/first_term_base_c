# План 
# Кратчайшие пути из одной вершины до остальных
# Сборка

# Дейкстра
Идея: В любой момент времени будем поддерживать два множества: S - множество
вершин, до которых уже найден кратчайший путь, U = V ∖ S - вершины,
кратчайший путь до которых еще неизвестен.
Также будем поддерживать массив d: d[v] хранит длину кратчайшего пути до v,
проходящего только по вершинам из S.

Алгоритм:
- Инициализация: S = {s}, d[v] = w(s, v), d[s] = 0
- Ищем u = argmin(d[v])
- Добавляем u в S, обновляем расстояния: d[v] = min(d[v], d[u] + w(u, v)). 
- Продолжаем пока S != V

Реализация:
- на очереди
- на массиве
- почти не отличается от Примы

Ограничение: веса должны быть не отрицательны

# Беллман-Форд

Релаксация ребра:
```c++
bool Relax(v, u, weight) {
    if (dist[u] > dist[v] + weight) {
        dist[u] = dist[v] + weight;
        parent[u] = parent[v];
        return true;
    }
    return false;
}
```

Алгоритм: 
Любой кратчайший путь состоит из не более V − 1 ребра (если нет циклов
отрицательного веса), поэтому для нахождения всех путей достаточно V − 1 раз отрелаксировать все ребра.

```c++
for (int i = 0; i < |V| - 1; ++i) {
    for (edge : edges) {
        relax(v, u, weight)
    }
}
```

Поиск циклов:
сделаем ещё одну итерацию и если получилось отрелаксировать ребро до вершины v, то это вершина либо состоит в отрицательном цикле, либо достижима из него. Чтобы найти вершину, которая лежит на цикле, можно |V|−1
 раз пройти назад по предкам из вершины v
. Так как наибольшая длина пути в графе из |V|
 вершин равна |V|−1
, то полученная вершина u
 будет гарантированно лежать на отрицательном цикле.

Зная, что вершина u
 лежит на цикле отрицательного веса, можно восстанавливать путь по сохраненным вершинам до тех пор, пока не встретится та же вершина u
. Это обязательно произойдет, так как в цикле отрицательного веса релаксации происходят по кругу.

# SPFA (Shortest Path Faster Algorithm)
Идея:
Давайте релаксировать только те ребра, которые ведут из вершин с
обновившимся значением dist.

Будем добавлять в очеред вершины, для которых входящее ребро прорелаксировало (если их там нет).
```c++
def SPFA(G, s):
    dist = [inf, ..., s: 0, ..., inf]
    prev = [None, ..., None]
    queue = {s}
    while queue is not empty:
        v = queue.pop()
        for u in G.neighbors(v):
            if Relax(v, u) and u not in queue:
                queue.push(u)
```
 Отрицательные циклы?
 
 # Сборка

 ## Makefile
В какой-то момент набирать ручками все файлы, которые хочешь собрать начинает быть утомительным. Тогда можно сконфигурировать так называемый make-file, который будет собирать то, что описано в нём. А подробнее: если выполнить команду `make`, то программа попытается найти файл с именем по умолчание Makefile в текущем каталоге и выполнить инструкции из него. `make -f MyMakefile` эта же команда выполнит конкретный MyMakefile. \
Теперь разберёмся как выглядит сам Makefile:
Самый простой выглядит так:
```
цель: зависимости
[tab] команда
```
```
all: main.cpp
    g++ main.cpp
```
Посложнее уже так:
```
all: hello

hello: main.o factorial.o hello.o
	g++ main.o factorial.o hello.o -o hello

main.o: main.cpp
	g++ -c main.cpp

factorial.o: factorial.cpp
	g++ -c factorial.cpp

hello.o: hello.cpp
	g++ -c hello.cpp

clean:
	rm -rf *.o hello
```
Чтобы запустить конкретную цель clean, то нужно вызвать такую команду: `make -f Makefile-2 clean`. По дефолту собирается команда `all`


## Cmake
Но и дальше мы хотим автоматизировать процесс сборки и для этого есть Cmake. Тут мы уже пишем файлы CmakeLists.txt и запускается это командой `cmake`. Cmake-files так устроены, что они сами генерируют Makefiles для сборки того, что в них указано. Затем запускаем make-files уже командой `make`.

Пример:

hello.cpp:
```c++
#include <iostream>

int main() {
    std::cout << "Hello, World!\n";
    return 0;
}
```

CMakeLists.txt:
```bash
cmake_minimum_required(VERSION 3.16) # Проверка версии CMake.
									# Если версия установленой программы
									# старее указаной, произайдёт аварийный выход.

add_executable(hello hello.cpp)		# Создает исполняемый файл с именем main
									# из исходника hello.cpp
```
Запуск:
```
mkdir tmp // создаём временную директорию для файлов, которые генерирует cmake
cd tmp
cmake .. // Запускаем сборку исходников из папки ..
make // Запускаем получившийся MakeFile
./hello // Запускаем программу

```

 [Статья с хабра](https://habr.com/ru/post/155467/)
