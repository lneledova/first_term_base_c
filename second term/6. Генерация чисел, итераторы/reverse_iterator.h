#ifndef REVERSE_ITERATOR
#define REVERSE_ITERATOR

#include <iterator>
#include <functional>

template <class Iterator>
class ReverseIterator {
  private:
    Iterator iterator_;

  public:
    using Reference = typename std::iterator_traits<Iterator>::reference;
    using Pointer = typename std::iterator_traits<Iterator>::pointer;
    using IteratorCategory = typename std::iterator_traits<Iterator>::iterator_category;
    using ValueType = typename std::iterator_traits<Iterator>::value_type;
    using DifferenceType = typename std::iterator_traits<Iterator>::difference_type;


    ReverseIterator() = default;
    ReverseIterator(Iterator iter) : iterator_(std::move(iter)) {};

    Iterator Base() const {
      return iterator_;
    };

    Reference operator*() const {
      return *(std::prev(iterator_));
    };

    Pointer operator->() const {
      return std::prev(iterator_).operator->();
    };

    Reference operator[](size_t idx) const {
      return *(std::prev(iterator_, idx + 1));
    };

    ReverseIterator& operator++() {
      iterator_ = std::prev(iterator_);
      return *this;
    };

    ReverseIterator operator++(int) {
      auto copy = *this;
      ++(*this);
      return copy;
    };

    ReverseIterator& operator--() {
      ++iterator_;
      return *this;
    };

    ReverseIterator operator--(int) {
      auto copy = *this;
      --(*this);
      return copy;
    };

    ReverseIterator& operator+=(size_t idx) {
      iterator_ = std::prev(iterator_, idx);
      return *this;
    };

    ReverseIterator& operator-=(size_t idx) {
      std::advance(iterator_, idx);
      return *this;
    };

    bool operator==(const ReverseIterator& other) const {
      return this->iterator_ == other.iterator_;
    };

    bool operator!=(const ReverseIterator& other) const {
      return !((*this) == other);
    };

    bool operator<(const ReverseIterator& other) const {
      return this->iterator_ > other.iterator_;
    };

    bool operator<=(const ReverseIterator& other) const {
      return (*this) == other || (*this) < other;
    };

    bool operator>(const ReverseIterator& other) const {
      return !((*this) <= other);
    };

    bool operator>=(const ReverseIterator& other) const {
      return (*this) == other || (*this) > other;
    }

    ReverseIterator operator+(int idx) const {
      auto copy = *this;
      copy += idx;
      return copy;
    };

    ReverseIterator operator-(size_t idx) const {
      auto copy = *this;
      copy -= idx;
      return copy;
    };

    DifferenceType operator-(const ReverseIterator& other) const {
      return std::distance(this->iterator_, other.iterator_);
    };

};

template <class Iterator>
ReverseIterator<Iterator> operator+(int idx, ReverseIterator<Iterator> iter) {
  return iter += idx;
}


template <class Iterator>
ReverseIterator<Iterator> MakeReverseIterator(Iterator iter) {
  return ReverseIterator<Iterator>(std::move(iter));
}

#endif
