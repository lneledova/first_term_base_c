#include <iostream>
#include <cstring>

struct Node {
  Node* prev = nullptr;
  int value;
};

struct Stack {
  Node* last = nullptr;
  size_t size = 0;
};

void Push(Stack* stack, int val) {
  auto new_node = new Node;
  new_node->value = val;
  new_node->prev = stack->last;
  stack->last = new_node;
  ++stack->size;
}

int Pop(Stack* stack) {
  if (stack->size == 0) {
    return 0;
  }
  int temp = stack->last->value;
  Node* last_for_delete = stack->last;
  stack->last = stack->last->prev;
  delete last_for_delete;
  --stack->size;
  return temp;
}

int Back(Stack* stack) {
  if (stack->size == 0) {
    return 0;
  }
  return stack->last->value;
}

int Size(Stack* stack) {
  return stack->size;
}

void Clear(Stack* stack) {
  if (stack->size > 0) {
    Pop(stack);
    Clear(stack);
  }
  delete stack->last;
}

int main() {
  int n;
  auto* command = new char[7];
  auto* stack = new Stack;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    std::cin >> command;
    if (std::strcmp("push", command) == 0) {
      int value;
      std::cin >> value;
      Push(stack, value);
      std::cout << "ok\n";
    } else if (std::strcmp("pop", command) == 0) {
      std::cout << Pop(stack) << '\n';
    } else if (std::strcmp("back", command) == 0) {
      std::cout << Back(stack) << '\n';
    } else if (std::strcmp("size", command) == 0) {
      std::cout << Size(stack) << '\n';
    } else if (std::strcmp("clear", command) == 0) {
      Clear(stack);
      std::cout << "ok\n";
    } else if (std::strcmp("exit", command) == 0) {
      std::cout << "bye\n";
      break;
    }
  }
  Clear(stack);
  delete stack;
  delete[] command;
}
