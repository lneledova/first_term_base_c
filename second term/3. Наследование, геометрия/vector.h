#ifndef VECTOR_H
#define VECTOR_H

#include <cstdint>

namespace geometry {
  class Vector {
    private:
      int64_t coord_x_;
      int64_t coord_y_;

    public:
  };


  Vector operator+(const Vector& left, const Vector& right);
}

#endif