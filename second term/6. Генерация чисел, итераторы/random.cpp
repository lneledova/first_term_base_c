#include "random.h"

double NormalProbability(double low, double high, size_t n_samples, std::mt19937_64& generator) {
  auto distr = std::normal_distribution<double>(); 
  int counter = 0;
  for (size_t i = 0; i < n_samples; ++i) {
    double value = distr(generator);
    counter += (value > low && value < high);
  }
  return static_cast<double>(counter) / (n_samples);
}

double MonteCarloPi(size_t n_samples, std::mt19937_64& generator) {
  auto distr = std::uniform_real_distribution<double>(-1.0, 1.0);
  int counter = 0;
  for (size_t i = 0; i < n_samples; ++i) {
    double x = distr(generator);
    double y = distr(generator);
    counter += ((x * x + y * y) <= 1.0);
  }
  return 4 * static_cast<double>(counter) / (n_samples);
}
