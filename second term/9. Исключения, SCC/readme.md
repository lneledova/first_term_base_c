# План
## Исключения
## Топологическая сортировка
## Компоненты сильной связности


# Исключения
Исключения это специальные объекты для обработки ошибок при исполнении программы. При возникновении ситуации, которая порождает ошибку исключение "выбрасывают":

```c++
  Rational& operator/=(Rational other) {
    if (other.numerator_ == 0) {
      throw RationalDivisionByZero{};
    }
    if (other.numerator_ < 0) {
      other.numerator_ = -other.numerator_;
      numerator_ = -numerator_;
    }
    other = {other.denominator_, other.numerator_};
    return operator*=(other);
  }

  void func() {
    Rational first(2), second(0);
    first /= second;
    std::cout << "Did division";
  }

```
При этом работа функции прекращается, для объектов на стеке
вызываются деструкторы. Та же участь постигает остальные функции
(раскручивание стека), то есть всю последовательность вызовов, приведшую к
ошибке. 

Для ловли же исключений существуют `try-catch` блоки:

```c++
try {
  rational /= zero;
} catch (RationalDivisionByZero ex) {
  // обработка исключения
}
```
Возникающее в блоке `try` исключение может быть поймано в `catch` блоке
соответствующего типа.

По завершении блока `catch` исключение считается успешно обработанным и
выполнение программы продолжается в нормальном режиме.

Одному блоку `try` может соответствовать несколько блоков `catch`:
```c++
int main() {
    try {
        Divide(1, 0); // бросает int
    } catch (double err) {
        std::cout << "double\n";
    } catch (int err) {
        std::cout << "int\n";
    }
    return 0;
}
```
В этом случае отработает только 1 блок, соответствующий типу брошенного
исключения. Если нужный блок `catch` не найден, то исключение поймано не будет. 
Блок `catch` выбирается только по точному соответствию. То есть приведений
типов не происходит. Кроме двух ситуаций:
- `void*` может поймать любой указатель.
- Приведения по иерархии наследования вверх (к родителям) работают.

Срабатывает всегда первый подходящий catch.

Заметим, что если мы выделили память, а затем вылетело исключение, то нам нужно как-то её освободить

```c++
void func() {
  auto ptr = new int;
  try {
    Divide(2, 0);
  } catch (...) { // словить любое исключение
    delete ptr;
    throw; // прокинуть исключение дальше
  }
  delete ptr;
}
```
При использовании же классов-обёрток не нужно об этом думать и код становится намного проще:
```c++
void func() {
    std::unique_ptr ptr = new int;
    Divide(2, 0);
}

```
### noexcept
- Спецификатор: указываем компилятору, что функция не бросает исключений. В случае исключения программа завершится аварийно
```c++
int f() noexcept {};
```
- Условный спецификатор: аналогично, но добавляется условие, когда функция небросающая
- Операция `noexcept`: определяет является ли выражение `noexcept`


## Типы рёбер

Если после алгоритма DfsVisit оставить только посещенные ребра и ориентировать их в
направлении обхода, то будет образовано дерево обхода. Таким образом после отработанного DFS все ребра можно разделить по категориям:
- Ребро дерева - ребро, которое принадлежит какому-то дереву обхода.
- Обратное ребро - ребро, ведущее из потомка в предка в некотором дереве.
- Прямое ребро - ребро, ведущее из предка в потомка, не являющегося сыном.
- Перекрестное ребро - ребро, соединяющее вершины не связанные
отношением "предок-потомок" (все остальные ребра).

 # Топологическая сортировка
 Топологическая сортировка (англ. topological sort) ориентированного ациклического графа $G=(V,E)$ представляет собой упорядочивание вершин таким образом, что для любого ребра $(u,v)∈E(G)$ номер вершины $u$ меньше номера вершины $v$.

 Любой ли граф можно топологически отсортировать?

 Алгоритм:
- Запустим DFS
-  В момент завершения работы над вершиной (перед выходом из DfsVisit)
положим вершину в начало списка.
- Так мы ничего не испортим, так как у добавленной вершины либо нет
исходящих ребер, либо все ребра ведут в черные вершины (значит они уже в
списке).
- Продолжаем пока не обойдем все вершины

Сложность: ?

# Компоненты сильной связности
Ориентированный граф называется сильно связным, если из любой вершины
существует путь до любой другой.

Компонента сильной связности - максимальный по включению сильно связный
подграф (или класс эквивалентности по отношению взаимной достижимости).

Конденсацией ориентированного графа $G = (V ,E)$ называется граф
$SCC(G) = (V ′,E ′)$ такой, что $V ′$ состоит из компонент сильной связности
графа G, а ребро $(A,B) ∈ E ′$, если $∃a ∈ A, b ∈ B : (a, b) ∈ E$.

Алгоритм поиска компонент сильной связности:

- Запустим TopSort без проверки на ацикличность.
- Транспонируем граф.
- Запустим DFS в порядке, полученном на шаге 1
- Найденные на шаге 3 компоненты - компоненты сильной связности.

Почему работает:

Лемма: \
Пусть A и B компоненты сильной связности графа G и в SCC(G) есть ребро
A в B. Запустим алгоритм топологической сортировки на графе G, но без
проверки на ацикличность. Тогда найдется вершина a ∈ A, которая будет
располагаться левее любой вершины из B. \
Доказательство: ...

Сложность: ?
