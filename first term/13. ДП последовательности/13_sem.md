# Перегрузка операций
В общем виде сигнатура выглядит так: `operator<символ оператора>(args)`.

```
// В виде внешней функции
Complex operator+(Complex x, Complex y) {
    return {x.re + y.re, x.im + y.im};
}

// В виде метода класса
// В этом случае текущий объект автоматически является левым
//операндом, и в качестве аргумента достаточно передать только правый (при
//необходимости)
Complex Complex::operator+(Complex y) const {
    return {re + y.re, im + y.im};
}
```
## Некоторые правила и особенности
- Хотя бы один из операндов должен быть пользовательского типа.
- Нельзя вводить новые операции в язык
- Нельзя менять арность и приоритет операций.
- Нельзя переопределять операции `::` , `.` , `?:` , `.*` , `sizeof` .
- Операции `=` , `()` , `[]` , `->` могут быть перегружены только в виде методов.
- Операции `&&` и `||` теряют свойство "короткого вычисления", (до C++17
теряли строгий порядок вычисления вместе с операцией `,`).

### Префиксный и постфиксный ++

```
// Возвращает ссылку на элемент: ++value
Complex& operator++(Complex& value) {
    ++value.re;
    return value;
}

// Возвращает prvalue: value++
Complex operator++(Complex& value, int) {
    auto old_value = value;
    ++value.re;
    return old_value;
}
```

### Операторы побитового сдвига(ввода/вывода)

```
class Complex {
    double re_;
    double im_;

    // Ключевое слово friend даёт доступ к приватным полям
    friend std::ostream& operator<<(std::ostream& os, Complex& complex) {
        os << value.re << " + " << value.im << 'i';
        return os;
    }
};  
```

# Динамика!

## Расстояние Левенштейна
Расстояние Левенштейна (также редакционное расстояние или дистанция редактирования) между двумя строками в теории информации и компьютерной лингвистике — это минимальное количество операций вставки одного символа, удаления одного символа и замены одного символа на другой, необходимых для превращения одной строки в другую.

Будем считать, что элементы строк нумеруются с первого, как принято в математике, а не нулевого.

Пусть $S_1$ и $S_2$ — две строки (длиной $M$ и $N$ соответственно) над некоторым алфавитом, тогда редакционное расстояние $d(S_1,S_2)$ можно подсчитать по следующей рекуррентной формуле:
```
1. D(0, 0) = 0 // расстояние между двумя пустыми строками равно нулю.
2. D(i, 0) = i * delete_cost // чтобы получить пустую строку из строки длиной i, нужно совершить i операций удаления
3. D(0, j) = j * insert_cost // чтобы получить строку из j символов из пустой, то нужно совершить j вставок
4. D(i, j) = min(D[i - 1][j] + delete_cost, D[i][j - 1] + insert_cost, D[i - 1][j - 1] + replace_cost) // S_1[i]≠S_2[j]
5. D(i, j) = D(i - 1, j - 1) // S_1[i] = S_2[j]
```

```
int levensteinInstruction(String s1, String s2, int InsertCost, int DeleteCost, int ReplaceCost):
  D[0][0] = 0
  for j = 1 to N
    D[0][j] = D[0][j - 1] + InsertCost                  
  for i = 1 to M
    D[i][0] = D[i - 1][0] + DeleteCost                  
    for j = 1 to N
      if S1[i] != S2[j] 
         D[i][j] = min(D[i - 1][j] + DeleteCost,        
                       D[i][j - 1] + InsertCost,                      
                       D[i - 1][j - 1] + ReplaceCost)                 
      else 
         D[i][j] = D[i - 1][j - 1]
  return D[M][N]
```

## НОП
```
int LCS(x: vector, y: vector):
   m = length(x)
   n = length(y)
   for i = 1 to m
     lcs[i][0] = 0
   for j = 0 to n
     lcs[0][j] = 0
   for i = 1 to m
     for j = 1 to n
       if x[i] == y[j]
         lcs[i][j] = lcs[i - 1][j - 1] + 1
         prev[i][j] = pair(i - 1, j - 1)
       else
         if lcs[i - 1][j] >= lcs[i][j - 1]
           lcs[i][j] = lcs[i - 1][j]
           prev[i][j] = pair(i - 1, j)
         else
           lcs[i][j] = lcs[i][j - 1]
           prev[i][j] = pair(i, j - 1)
 
  // вывод LCS, вызывается как printLCS(m, n)
 int printLCS(i: int, j: int):
   if i == 0 or j == 0  // пришли к началу LCS
     return
   if prev[i][j] == pair(i - 1, j - 1)  // если пришли в lcs[i][j] из lcs[i - 1][j - 1], то x[i] == y[j], надо вывести этот элемент
     printLCS(i - 1, j - 1)
     print x[i]
   else
     if prev[i][j] == pair(i - 1, j)
       printLCS(i - 1, j)
     else
       printLCS(i, j - 1)
```

## Максимальный подпалиндром
Обозначим данную последовательность через S, а ее элементы — через S[i], $0\leq i \leq n−1$, где n — длина строки S. Будем рассматривать возможные подпоследовательности данной последовательности с i−го по j−ый символ включительно, обозначив её как S(i,j). Длины максимальных подпалиндромов для данной последовательности будем записывать в двумерный массив L: L[i][j] — длина максимальной подпоследовательности-палиндрома, который можно получить из последовательности S(i,j). 
```
1. L(i, i) = 1 // палиндром из одного элемента.
2. L(i, j) = 0 // i > j
3. L(i, j) = L[i + 1][j − 1] + 2 // s[i] = s[j]
4. L(i, j) = max(L[i][j − 1], L[i + 1][j]) // S_1[i] != S_2[j]
```
Перед вызовом процедуры заполняем L[][] начальными значениями: L[i][j] = 1 если i = j, L[i][j] = 0, если i>j, в остальных случаях L[i][j]=−1. При первом вызове функции в качестве аргументов передаем индексы первого и последнего элементов исходной строки. Например для строки длиной N вызов функции будет иметь следующий вид: palSubSeq(0,N − 1). Искомая же длина будет записана в ячейке L[0][N−1].
Подсчёт длины максимального подпалиндрома
```
 int palSubSeq(left: int, right: int):
   if L[left][right] == -1 
     if s[left] == s[right] 
       L[left][right] = palSubSeq(left + 1, right - 1) + 2
       prev(left, right) = pair(left + 1, right - 1)
     else 
       int left_pal = palSubSeq(left + 1, right)
       int right_pal = palSubSeq(left, right - 1)
       if (left_pal > right_pal)
            L[left][right] = left_pal
        else
            L[left][right] = right_pal
   return L[left][right]
```

Поиск палиндрома
```
palChars(left: int, right: int, palLeft: int, palRight: int):
   while left > right
     if left == right and L[left][right] == 1
       palindrome[palLeft++] = S[left++]
     else
       if S[left] == S[right]
         palindrome[palLeft++] = S[left++]
         palindrome[palRight--] = S[right--]
       else
         if L[left + 1][right] > L[left][right - 1]
           left++
         else
           right--
```
