
## [Тест по 7 семинару](https://forms.gle/28gMq5aAyMGTncev7)
## [Optional](https://contest.yandex.ru/contest/47810/problems/L*/) 
## Выравнивание памяти
Рассмотрим такую структуру:
```c++
struct A {
    char x; // 1 байт
    int y;  // 4 байта
    char z; // 1 байт
};

int main() {
    std::cout << sizeof(A) << '\n'; // Сколько выведет?
}
```
Почему так выходит? \
Ответ: выравнивание памяти \
Память компьютера работает следующим образом. Память программы можно представить себе как большой массив байт, а адрес — индекс в этом массиве.

```
[байт 0] [байт 1] [байт 2] [байт 3] [байт 4] [байт 5] [байт 6] [байт 7]...
```
Байт, как минимально адресуемую единицу памяти, можно адресовать как угодно. Но обычно вы работаете с более крупными единицами. Например, это машинное слово: размер регистра процессора. Для 32-битной архитектуры это четырёхбайтовая структура.

Теперь, байты прекрасно организовываются в четвёрки:
```
[байт 0] [байт 1] [байт 2] [байт 3] [байт 4] [байт 5] [байт 6] [байт 7]...
[        машинное слово 0         ] [            машинное слово 1     ]...
```
Но заметьте, что в принципе байты можно бы комбинировать в машинные слова и по-другому:
```
[байт 0] [байт 1] [байт 2] [байт 3] [байт 4] [байт 5] [байт 6] [байт 7] [байт 8]...
         [          машинное слово         ] [     ещё одно машинное слово     ]...
```
машинное слово может начинаться по любому адресу!

Так вот, большинство компьютеров устроено так, что машинные слова, скомбинированные в четвёрки верхним образом читаются быстро, а вот машинные слова второго типа (то есть, те, которые не получаются шагами по 4 от нуля) — медленно. (Хотя требования по выравниванию могут быть и сложнее, и не совпадать с кратностью машинного слова.) Хуже того, очень многие архитектуры вовсе не позволяют читать такие слова, и для того, чтобы прочитать вот такое слово:
```
[байт 0] [байт 1] [байт 2] [байт 3] [байт 4] [байт 5] [байт 6] [байт 7]...
         [        машинное слово           ]        
```
приходится читать машинные слова по адресу 0 и 4
```
[байт 0] [байт 1] [байт 2] [байт 3] [байт 4] [байт 5] [байт 6] [байт 7]...
        [    нужно это машинное слово      ]
[     но приходится читать это    ] [              и этo              ]
```
выбирать из них нужные байты, перетасовывать и собирать «вручную» в нужное машинное слово! Понятно, что это сложная и сравнительно дорогостоящая операция.

Поэтому компиляторы языка программирования C (да и большинства других тоже) проводят такую оптимизацию: между полями структуры вставляются незначащие байты для того, чтобы у всех полей было хорошее выравнивание.
```
struct A {
    char x; // 1 байт
    int y;  // 4 байта
    char z; // 1 байт
};

[    ] [   ] [    ] [     ] [    ] [    ] [    ] [    ] [    ] [   ] [    ] [     ]
[  x ] { потерянные байты } [             y           ] [  z ] { потерянные байты }
```
Если сама структура будет выровнена в памяти (об этом компилятор тоже заботится), то y будет выровнено, и доступ к нему будет быстрым (а на некоторых платформах, напомню, вообще только в этом случае возможен).

Но в этом случае наша структура занимает больше памяти, чем если бы порядок переменных был таким:

```
[    ] [    ] [    ] [    ] [    ] [    ] [    ] [    ]
[  x ] [  z ] {  потеряно } [             y           ]
```
Бóльшая структура означает бóльший расход памяти и бóльшие затраты на копирование, чтение этой структуры и тому подобное. Таким образом, переставив поля структуры, мы можем сэкономить.

Обратите внимание, что на самом деле универсальной оптимизации не существует. Например, для разных архитектур компьютера размер машинного слова будет отличаться. Кроме того, и размеры сколько-нибудь сложных данных могут отличаться тоже, а значит вычисленный оптимальный порядок на одной системе может оказаться очень неоптимальным на другой системе.

Можно явно указать компилятору, что вашу структуру нужно упаковать так, чтобы занимать минимально памяти, для этого используется атрибут `packed`:

```c++
struct PackedA {
    char x; // 1 байт
    int y;  // 4 байта
    char z; // 1 байт
} __attribute__((packed));
```
У типов в С++ есть свойство выравнивание ([aligment required](https://en.cppreference.com/w/cpp/language/object#Alignment)), которое представляет собой расстояние между адресами байт, по которым могут размещаться объекты данного типа. Также выравнивание всегда является степенью двойки. Это требование возникает как раз из-за того, что читать несколько бит для процессора быстрее, когда они скомпонованы так:
```
[байт 0] [байт 1] [байт 2] [байт 3] [байт 4] [байт 5] [байт 6] [байт 7]...
[   переменная типа int 1              ] [             переменная типа int 2    ]...
```
То есть для типа int это будет 4, для char 1. Данное свойство для типа можно узнать с помощью `alignof`:
```c++
struct A {
    char x; // 1 байт
    int y;  // 4 байта
    char z; // 1 байт
};

struct PackedA {
    char x; // 1 байт
    int y;  // 4 байта
    char z; // 1 байт
} __attribute__((packed));

std::cout << alignof(int) << '\n';
std::cout << alignof(A) << '\n'; // Ваши предположения?
std::cout << alignof(PackedA) << '\n';
```
Для структуры выравнивание по умолчанию устанавливается как максимум выравниваний типов её полей. \
Также есть ключевое слово `alignas`, которое устанавливает выравнивание для объекта, но нельзя передать аргумент меньше значения по умолчанию или не степень двойки:
```c++
struct alignas(25) A { // CE
//
};

struct alignas(1) A { // CE
    char x; // 1 байт
    int y;  // 4 байта
    char z; // 1 байт
};

struct alignas(32) A { // OK
    char x; // 1 байт
    int y;  // 4 байта
    char z; // 1 байт
};
```
## DFS
Алгоритм DFS:
- Берем вершину, проходим из нее по ребру в произвольную еще не
посещенную вершину.
- Если из очередной вершины некуда идти, то возвращаемся в предыдущую и
продолжаем обход.
- Если вернулись в стартовую вершину и из нее некуда идти, то заканчиваем.

В процессе работы алгоритма для каждой вершины будем дополнительно
хранить следующие величины:
- Цвет:
    - Белый - вершины еще не посещена.
    - Серый - из вершины осуществляется обход в глубину.
    - Черный - вершина была обнаружена и из нее нет доступных путей.
- time_in: момент первого обнаружения вершины
- time_out: момент завершения обхода из вершины

Заметим, что если оставить только посещенные ребра и ориентировать их в
направлении обхода, то DfsVisit образует дерево обхода.

После отработанного DFS все ребра можно разделить по категориям:
- Ребро дерева - ребро, которое принадлежит какому-то дереву обхода.
- Обратное ребро - ребро, ведущее из потомка в предка в некотором дереве.
- Прямое ребро - ребро, ведущее из предка в потомка, не являющегося сыном.
- Перекрестное ребро - ребро, соединяющее вершины не связанные
отношением "предок-потомок" (все остальные ребра).

Как определить тип ребра в алгоритме:
- Ребро дерева: ребро ведущее в белую вершину.
- Обратное ребро: ребро ведущее в серую вершину.
- Прямое ребро: ребро vu ведущее в черную вершину,
у которого time_in[v] < time_in[u]
- Перекрестное ребро: ребро vu ведущее в черную вершину,
у которого time_in[v] > time_in[u]

Давайте докажем, что в неориентированных графах есть только
древесные и обратные ребра.
