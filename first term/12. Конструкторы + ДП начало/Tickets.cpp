#include <iostream>
#include <cstddef>

int64_t Min(int64_t first, int64_t second) {
  return first < second ? first : second;
}

int64_t FindMinTime(int64_t* a_array, int64_t* b_array, int64_t* c_array, int n) {
  if (n == 1) {
    return a_array[0];
  }
  if (n == 2) {
    return Min(a_array[0] + a_array[1], b_array[0]);
  }
  auto dp = new int64_t[n];
  dp[0] = a_array[0];
  dp[1] = Min(a_array[0] + a_array[1], b_array[0]);
  dp[2] = Min(c_array[0], Min(dp[0] + b_array[1], dp[1] + a_array[2]));

  for (int i = 3; i < n; ++i) {
    dp[i] = Min(a_array[i] + dp[i - 1], Min(b_array[i - 1] + dp[i - 2], c_array[i - 2] + dp[i - 3]));
  }

  int64_t res = dp[n - 1];
  delete[] dp;
  return res;
}

int main() {
  int n;
  std::cin >> n;
  auto a_array = new int64_t[n];
  auto b_array = new int64_t[n];
  auto c_array = new int64_t[n];

  for (int i = 0; i < n; ++i) {
    std::cin >> a_array[i] >> b_array[i] >> c_array[i];
  }

  std::cout << FindMinTime(a_array, b_array, c_array, n);

  delete[] a_array;
  delete[] b_array;
  delete[] c_array;
}
